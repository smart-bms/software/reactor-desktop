# Reactor Desktop

An application to monitor and configure your reactor batteries

![Screenshot of Application](https://gitlab.com/smart-bms/software/reactor-desktop/-/raw/master/data/screenshots/Screenshot%20from%202020-11-03%2016-18-10.png) <!--TODO-->

## Building, Testing, and Installation

You'll need the following dependencies:

- libhandy-1-dev
- libgee-0.8-dev
- libgtk-3-dev

Also `meson` (and, by extension, `ninja`) is a requirement to build the application, since it's the building
system used in this app.

> <h4>Regressions</h>
> It's important to know that the building process temporarily pulls more
> dependencies on its own from the internet (specifically `libreactor-com` 
> and `libreactor-gobject`, which are planned to be packaged more properly in
> the future). Because of this the following actions are unavailable: 
>
> - building this application in a containerized 
> environment.
>
> - running the installed application.

To build the application:

	meson build
	cd build
	ninja

You'll find the executable inside the `build/src`. To run the app go to the
`build/src` and run:

	./reactor-desktop

<!--TODO: Add Debugging functionality and options-->
