namespace ReactorDesktop.Model {
	
	public class Receiver {

		public signal void response (int response_id);

		internal void response_func (int response_id) {
			response (response_id);
		}
			
	}
	
}
