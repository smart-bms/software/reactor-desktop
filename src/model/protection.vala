namespace ReactorDesktop.Model {

    public class Protection: Analysable<Reactor.ComProtection> {

        public Reactor.ComProtection flags;

        public Protection (Reactor.ComProtection flags) {
            this.flags = flags;
        }

        public Protection.from_uint (uint flags) {
            this ((Reactor.ComProtection) flags);
        }

        protected Reactor.ComProtection get_flags () {
            return flags;
        }

        public string to_string () {
            switch (flags) {
                case Reactor.ComProtection.CELL_UV: return "Cell Undervoltage";
                case Reactor.ComProtection.CELL_OV: return "Cell Overvoltage";
                case Reactor.ComProtection.CHARGING_OT: return "Charging Overtemperature";
                case Reactor.ComProtection.CHARGING_UT: return "Charging Undertemperature";
                case Reactor.ComProtection.DISCHARGING_OT: return "Discharging Overtemperature";
                case Reactor.ComProtection.DISCHARGING_UT: return "Discharging Undertemperature";
                case Reactor.ComProtection.CHARGING_OC: return "Charging Overcurrent";
                case Reactor.ComProtection.DISCHARGING_OC: return "Discharging Overcurrent";
                default: return "FIXME: Invalid Protection Flag";
            }
        }
    }

}
