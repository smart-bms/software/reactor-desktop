namespace ReactorDesktop.Model {

    public class Status: Analysable<Reactor.ComStatus> {

        public Reactor.ComStatus flags;

        public bool important {
            get {
                return Reactor.ComStatus.PROTECTED in flags;
            }
        }

        public ThemedIcon icon {
            owned get {
                switch (flags) {
                    case Reactor.ComStatus.CHARGING: return new ThemedIcon ("battery-full-charging-symbolic");
                    case Reactor.ComStatus.DISCHARGING: return new ThemedIcon ("battery-good-symbolic");
                    case Reactor.ComStatus.PROTECTED: return new ThemedIcon ("dialog-warning-symbolic");
                    case Reactor.ComStatus.LOCKED: return new ThemedIcon ("changes-prevent-symbolic");
                    default: return new ThemedIcon ("dialog-error-symbolic");
                }
            }
        }

        public Status (Reactor.ComStatus flags) {
            this.flags = flags;
        }

        public Status.from_uint (uint flags) {
            this ((Reactor.ComStatus) flags);
        }

        protected Reactor.ComStatus get_flags () {
            return flags;
        }

        public string to_string () {
            switch (flags) {
                case Reactor.ComStatus.CHARGING: return "Charging";
                case Reactor.ComStatus.DISCHARGING: return "Discharging";
                case Reactor.ComStatus.PROTECTED: return "Protected";
                case Reactor.ComStatus.LOCKED: return "Locked";
                default: return "FIXME: Invalid Status Flag";
            }
        }
    }

}
