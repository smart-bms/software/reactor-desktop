[GenericAccessors]
public interface Analysable<T> {

    protected abstract T get_flags ();

    public int[] analyse ()
        requires (typeof(T).is_enum() || typeof(T).is_flags())
    {
        var enum_class = (EnumClass) typeof(T).class_ref ();
        int[] found_values = {};
        var uint_value = (uint) get_flags ();
        foreach (var val in enum_class.values) {
            if ((val.value & uint_value) == val.value) {
				found_values += val.value;
            }
        }
        return found_values;
    }

}
