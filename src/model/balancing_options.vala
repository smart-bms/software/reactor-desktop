namespace ReactorDesktop.Model {

	public errordomain BalancingOptionsError {
		PARSE_FAILED
	}

	public enum BalancingOptions {
		
		ALWAYS, CHARGING, NEVER;
		
		public string to_string () {
			switch (this) {
				case ALWAYS: return "always";
				case CHARGING: return "charging";
				case NEVER: return "never";
				default: assert_not_reached ();
			}
		}
		
		public static BalancingOptions from_string (string str)
			requires (
				str == "always" || 
				str == "charging" ||
				str == "never"
			)
		{
			switch (str) {
				case "always": return BalancingOptions.ALWAYS;
				case "charging": return BalancingOptions.CHARGING;
				case "never": return BalancingOptions.NEVER;
				default: assert_not_reached ();
			}
		}
		
	}

}
