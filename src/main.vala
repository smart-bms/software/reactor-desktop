/* main.vala
 *
 * Copyright 2020 Alex Angelou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Hdy;

public class ReactorDesktop.Application: Gtk.Application {
	public class MessageHandler: Object {
	
		public signal Model.Receiver error 
				(string message, View.Widgets.InfoBar.ButtonInfo[] options);
	
		public static MessageHandler? _INSTANCE = null;
		public static MessageHandler INSTANCE {
			get {
				return _INSTANCE == null? (_INSTANCE = new MessageHandler ()): _INSTANCE;
			}
		}
		
		static construct {
			new Model.Receiver ();
		}
		
		private MessageHandler () {}

	}

	private Settings settings;

	public static MessageHandler broadcaster {
		get {
			return MessageHandler.INSTANCE;
		}
	}

	public Application () {
		Object (application_id: "energy.reactor.Desktop",
				flags: ApplicationFlags.FLAGS_NONE);
	}

	construct {
		settings = new Settings ("energy.reactor.Desktop");
	}

	public override void activate () {
		init ();

		if (active_window == null) {
			var window = new ReactorDesktop.View.Window (this);

			window.destroy.connect (() => {
				window.save_settings (settings);
			});
		}

		((ReactorDesktop.View.Window) active_window).load_settings (settings);
		active_window.present ();
	}
	
	void init () {
		Hdy.init ();
		ReactorDesktop.init ();
	}
}

int main (string[] args) {
	var app = new ReactorDesktop.Application ();
	return app.run (args);
}
