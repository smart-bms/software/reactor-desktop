namespace ReactorDesktop.View {

	[GtkTemplate (ui = "/com/reactor/Desktop/view/console_view.ui")]
	public class ConsoleView : ContentsView {
        public override Reactor.Com? communications {get; set;}

		[GtkChild]
		ReactorConsole.Terminal widget;

		construct {
			bind_property ("communications", widget, "com");
		}
	}
}
