/* window.vala
 *
 * Copyright 2020 Alex Angelou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ReactorDesktop.View {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		
		[GtkChild]
		Gtk.Stack page_stack;

		[GtkChild]
		Widgets.InfoBar message_bar;

		[GtkChild]
		Widgets.BatteryList battery_list;

		public Page active_page { get; set; }

		public Window (Gtk.Application app) { 
			Object (application: app);
		}
	
		construct {
			active_page = new Page ();
			active_page.visible = true;

			page_stack.add (active_page);

			ReactorDesktop.Application.broadcaster.error.connect ((message, options) => {
				return message_bar.error (message, options);
			});

			battery_list.changed.connect (() => {
				var device_path = battery_list.active_id;

				if (device_path != null) {
					var com = new Reactor.ComTty (device_path);

					active_page.communications = com;
				}

				message ("battery changed %s", battery_list.active_id);
			});
		}

		public void load_settings (Settings settings) {
			var rect = Gtk.Allocation ();

			settings.get ("window-allocation", "(iiii)",
						  out rect.x, out rect.y,
						  out rect.width, out rect.height);

			if (rect.x > 0 && rect.y > 0 && rect.width > 0 && rect.height > 0) {
				set_allocation (rect);
			}
			
			message (@"($(rect.x), $(rect.y) : $(rect.width) x $(rect.height))\n");

			if (settings.get_boolean ("window-maximized")) {
				maximize ();
			}
		}

		public void save_settings (Settings settings) {
			Gtk.Allocation rect;

			get_allocation (out rect);

			message ("save_settings");

			settings.set ("window-allocation",
						  "(iiii)",
						  rect.x, rect.y, rect.width, rect.height);
		}
	}
}
