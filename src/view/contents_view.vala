namespace ReactorDesktop.View {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/contents_view.ui")]
	public abstract class ContentsView : Gtk.Bin {

        [GtkChild]
        public Gtk.Box contents;
        
        [GtkChild]
        public Gtk.ScrolledWindow scrolled_window;

		public abstract Reactor.Com? communications {get; set;}

		public Gtk.Box header_widgets {
			get; protected set;
			default = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
		}

        public override void add (Gtk.Widget widget) {
            if (contents != null) {
                contents.pack_start(widget, true, true, 0);
                return;
            }

            base.add(widget);
        }

        construct {
        	header_widgets.visible = true;
            assert(contents != null);
        }

	}

	public class ContentsViewEmpty : ContentsView {
        
        public override Reactor.Com? communications {get; set;}
        
	}
}
