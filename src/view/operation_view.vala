using Gee;

namespace ReactorDesktop.View {

	[GtkTemplate (ui = "/com/reactor/Desktop/view/operation_view.ui")]
	public class OperationView : ContentsView {

        [GtkChild]
        Gtk.Label percentage_label;

        [GtkChild]
        Gtk.FlowBox status_box;

        [GtkChild]
        Gtk.FlowBox protection_box;

        [GtkChild]
        Gtk.Label current_label;

        [GtkChild]
        Gtk.Label power_label;
        
        [GtkChild]
        Gtk.Button charging_on_button;

        [GtkChild]
        Gtk.Button charging_off_button;

        [GtkChild]
        Gtk.Button discharging_on_button;

        [GtkChild]
        Gtk.Button discharging_off_button;

        [GtkChild]
        Gtk.Label capacity_nom_label;

        [GtkChild]
        Gtk.Label capacity_est_label;

        [GtkChild]
        Widgets.CellDashboard cell_dashboard;

        public Reactor.Com? _communications = null;
        public override Reactor.Com? communications {
            get {
                return _communications;
            }
            set {
                _communications = value;

            	Timeout.add (200, () => {
            		try {
            			value.send_query (
            				  Reactor.ComQuery.VOLTAGE		|
            				  Reactor.ComQuery.CURRENT		|
            				  Reactor.ComQuery.BALANCING	|
            				  Reactor.ComQuery.STATUS		|
            				  Reactor.ComQuery.PROTECTION
            			);
            		} catch (Error e) {
            			ReactorDesktop.Application.broadcaster.error (@"Failed request: $(e.message)", {});
            		}
            		return value == _communications;
            	});
            
                var polynomial = new Reactor.Polynomial
					.from_array ({0, 1/42.0f, 0, 0});

                cell_dashboard.communications = value;
                value.receive_voltage.connect ((voltage) => {
                    var avg = 0;
                    foreach (var v in voltage) {
                        avg += v;
                    }
                    avg /= voltage.length;
                    this.percentage = polynomial.compute (avg);
                });
                value.receive_status.connect ((status) => this.status = new Model.Status.from_uint (status));
                value.receive_protection.connect ((protection) => this.protection = new Model.Protection.from_uint (protection));
                value.receive_current.connect ((current) => this.current = current);
                // value.receive_power.connect ((power) => this.power = power); TODO
            }
        }

        float _percentage;
        public float percentage {
            get {
                return _percentage;
            }
            set {
                _percentage = value;
                percentage_label.label = value.to_string("%.1f") + "%";
            }
        }

		Reactor.ComStatus? status_cache = null;
        Model.Status _status;
        public Model.Status status {
            get {
                return _status;
            }
            set {
                _status = value;

				if (status_cache != null && value.flags == status_cache) {
					return;
				}
				
				status_cache = value.flags;

                foreach (var child in status_box.get_children ())
                    status_box.remove (child);

                foreach (var flag in value.analyse ()) {
                    add_status_flag (new Model.Status((Reactor.ComStatus) flag));
                }
            }
        }

		Reactor.ComProtection? protection_cache = null;
        Model.Protection _protection;
        public Model.Protection protection {
            get {
                return _protection;
            }
            set {
                _protection = value;

				if (protection_cache != null && value.flags == protection_cache) {
					return;
				}
				
				protection_cache = value.flags;

                foreach (var child in protection_box.get_children ())
                    protection_box.remove (child);

                foreach (var flag in value.analyse ()) {
                    add_protection_flag (new Model.Protection ((Reactor.ComProtection) flag));
                }
            }
        }

        uint _current;
        public uint current {
            get {
                return _current;
            }
            set {
                _current = value;
                current_label.label = value.to_string("%d") + " mA";
            }
        }

        float _power;
        public float power {
            get {
                return _power;
            }
            set {
                _power = value;
                power_label.label = value.to_string("%.1f") + " W";
            }
        }

        float _capacity_nom;
        public float capacity_nom {
            get {
                return _capacity_nom;
            }
            set {
                _capacity_nom = value;
                capacity_nom_label.label = value.to_string("%.1f") + " mAh";
            }
        }

        float _capacity_est;
        public float capacity_est {
            get {
                return _capacity_est;
            }
            set {
                _capacity_est = value;
                capacity_est_label.label = value.to_string("%.1f") + " mAh";
            }
        }

		construct {
			charging_on_button.clicked.connect (() => send_set_status (true, Reactor.ComStatus.CHARGING));
			charging_off_button.clicked.connect (() => send_set_status (false, Reactor.ComStatus.CHARGING));
			discharging_on_button.clicked.connect (() => send_set_status (true, Reactor.ComStatus.DISCHARGING));
			discharging_off_button.clicked.connect (() => send_set_status (false, Reactor.ComStatus.DISCHARGING));
		}
		
		private void send_set_status (bool setting, Reactor.ComStatus status) {
			if (communications == null)
				return;
			
			try {
				if (setting) {
					communications.send_set (status);
				} else {
					communications.send_reset (status);
				}
			} catch (Error e) {
				ReactorDesktop.Application.broadcaster.error (@"Failed to set status to $status: $(e.message)", {{"Retry", Gtk.ResponseType.YES}})
						.response.connect ((res) => {
							if (res == Gtk.ResponseType.YES) {
								send_set_status (setting, status);
							}
						});
			}
		}

		static Widgets.Flag flag_factory (Widgets.Flag flag) {
			flag.hexpand = false;
			flag.halign = Gtk.Align.END;
			return flag;
		}

        void add_status_flag (Model.Status status_flag) {
            status_box.add (flag_factory (new Widgets.StatusFlag (status_flag)));
        }

        void add_protection_flag (Model.Protection protection_flag) {
            protection_box.add (flag_factory (new Widgets.ProtectionFlag (protection_flag)));
        }
	}
}
