/* window.vala
 *
 * Copyright 2020 Alex Angelou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ReactorDesktop.View {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/page.ui")]
	public class Page : Gtk.Bin {

		[GtkChild]
		OperationView operation_box;

		[GtkChild]
		SettingsView settings_box;
		
		[GtkChild]
		ConsoleView console_box;

		[GtkChild]
		Hdy.Leaflet main_stack;

		[GtkChild]
		Gtk.Revealer separator_revealer;

		[GtkChild]
		Widgets.Title title;

		ContentsView[] views;

		public static Gtk.Stack dummy_stack {
			get;
			default = (Gtk.Stack) new Gtk.Builder
					.from_resource ("/com/reactor/Desktop/view/widgets/dummy_stack.ui")
					.get_object ("dummy_stack");
		}

		Gtk.Stack instructor_stack = dummy_stack;

		public string battery_name {
			get {
				return title.text;
			}
			set {
				title.text = value;
			}
		}

		public bool title_switcher_revealed {get; set;}

		public Gtk.Box header_bar_box {
			get; private set;
			default = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 8);
		}

		public Reactor.Com communications { get; set; }

		public virtual signal void folded (bool folded) {
			title_switcher_revealed = folded;
		}

		public Page () {
			bind_property ("communications", operation_box, "communications");
		}
		
		construct {
			header_bar_box.visible = true;
		
			instructor_stack.notify.connect (() => {
				if (main_stack.visible_child_name != instructor_stack.visible_child_name)
					main_stack.visible_child_name  = instructor_stack.visible_child_name;
			});

			main_stack.notify["folded"].connect ((param) => folded (main_stack.folded));
			
			views = {settings_box, operation_box, console_box};

			foreach (var view in views)
				connect_view (view);
		}
		
		void connect_view (ContentsView view) {
			view.scrolled_window.vadjustment.value_changed.connect (() => on_adjustment_changed ());

			var revealer = new Gtk.Revealer ();
			revealer.visible = true;
			revealer.transition_type = Gtk.RevealerTransitionType.CROSSFADE;
			revealer.add (view.header_widgets);

			folded.connect ((folded) => {
				revealer.reveal_child = !folded || main_stack.visible_child == view;
			});
			instructor_stack.notify.connect (() => {
				revealer.reveal_child = !main_stack.folded || main_stack.visible_child == view;
			});
			folded (main_stack.folded);
			header_bar_box.pack_end (revealer, false, false, 0);
		}
		
		bool scrolled_a_bit (ContentsView view) {
			var adjustment = view.scrolled_window.vadjustment;
			return adjustment.value != adjustment.lower;
		}
		
		void on_adjustment_changed () {
			if (main_stack.folded) {
				var visible_child = main_stack.visible_child as ContentsView;
				if (visible_child == null) return;
				separator_revealer.reveal_child = scrolled_a_bit (visible_child);
			} else {
				separator_revealer.reveal_child = 
				    scrolled_a_bit (operation_box) || 
				    scrolled_a_bit (settings_box) || 
				    scrolled_a_bit (console_box);
			}
		}
		
	}
}
