namespace ReactorDesktop {

    public void init () {
        Model.init ();
        View.init ();
    }

	namespace Model {
		
		public void init () {
			new Receiver ();
		}
		
	}

    namespace View {

        public void init () {
            Css.init ();
            Widgets.init ();
            new ContentsViewEmpty ();
            new OperationView ();
            new SettingsView ();
            new ConsoleView ();
        }

        namespace Widgets {

            public void init () {
                new ContentsRow ();
                new Title ("");
                new Flag ();
                new ModeSettings ();
                new BatteryList ();
                new InfoBar ();
                new CellDashboard.ValueBar ();
                new CellDashboard ();
                new Polynomial ();
                
                new ReactorConsole.Terminal ();
            }

        }
    }
}
