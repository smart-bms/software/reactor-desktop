/* title.vala
 *
 * Copyright 2020 Alex Angelou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ReactorDesktop.View.Widgets {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/widgets/title.ui")]
	public class Title : Gtk.Stack {

        [GtkChild]
        Gtk.Label label;

        [GtkChild]
        Gtk.Entry entry;

        [GtkChild]
        Gtk.Box label_box;

        [GtkChild]
        Gtk.Button edit_button;

        public string text {
            get {
                return label_box == visible_child? label.label: entry.text;
            }
            set {
                string new_str = value.to_string();
                label.label = new_str;
                entry.text = new_str;
            }
        }

        public Title (string text) {
            this.text = text;
        }

        construct {
            edit_button.clicked.connect (show_entry);
            entry.activate.connect (show_label);
            
//            var image = edit_button.image as Gtk.Image;
//            if (image != null)
//            	image.gicon = new ThemedIcon ("document-edit-symbolic");
        }

        public void show_label () {
            this.text = entry.text;
            visible_child = label_box;
        }

        public void show_entry () {
            this.text = label.label;
            visible_child = entry;
        }

	}
}
