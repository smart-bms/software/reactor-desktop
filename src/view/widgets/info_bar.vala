/* flag.vala
 *
 * Copyright 2020 Alex Angelou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using ReactorDesktop.Model;

namespace ReactorDesktop.View.Widgets {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/widgets/info_bar.ui")]
	public class InfoBar : Gtk.InfoBar {

		[GtkChild]
		Gtk.Label label;

		public struct ButtonInfo {
			public string text;
			public Gtk.ResponseType response_type;
		}

		protected new Receiver show (string text, ButtonInfo[] buttons) {
			var action_area = get_action_area ();
			action_area.forall ((widget) => action_area.remove (widget));

			label.label = text;

			foreach (var btn in buttons) {
				add_button (btn.text, btn.response_type);
			}

			var response_Receiver = new Receiver ();
			response.connect (response_Receiver.response_func);
			close.connect (() => response.disconnect (response_Receiver.response_func));

			revealed = true;
			return response_Receiver;
		}

		construct {
			show_close_button = true;
			response.connect ((i) => {
				switch (i) {
				case Gtk.ResponseType.CLOSE:
					close ();
					return;
				}
			});
			close.connect (() => revealed = false);
		}

		public Receiver error (string text, ButtonInfo[] buttons) {
			message_type = Gtk.MessageType.ERROR;
			return show (text, buttons);
		}
		
		public Receiver warning (string text, ButtonInfo[] buttons) {
			message_type = Gtk.MessageType.WARNING;
			return show (text, buttons);
		}

		public Receiver info (string text, ButtonInfo[] buttons) {
			message_type = Gtk.MessageType.INFO;
			return show (text, buttons);
		}

		public Receiver question (string text, ButtonInfo[] buttons) {
			message_type = Gtk.MessageType.QUESTION;
			return show (text, buttons);
		}

	}
}
