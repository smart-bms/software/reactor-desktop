namespace ReactorDesktop.View.Widgets {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/widgets/battery_list.ui")]
	public class BatteryList : Gtk.ComboBoxText {
		private FileMonitor file_monitor;
		private Gee.ArrayList<File> dev_files = new Gee.ArrayList<File> ();
	
		construct {
			foreach (var path in dev_files) {
				append_text (Path.get_basename (path.get_path ()));
			}

			try {
				var dev = Dir.open ("/dev/", 0);
				string? name;
				while ((name = dev.read_name ()) != null) {
					if (name.has_prefix ("ttyUSB") || name.has_prefix ("ttyACM")) {
						dev_files.add (File.new_for_path (@"/dev/$name"));
					}
				}
				
				dev_files.sort (path_sort_function);
				
				file_monitor = File
					.new_for_path ("/dev/")
					.monitor_directory (FileMonitorFlags.WATCH_MOVES);

				file_monitor.changed.connect ((src, dst, action_flag) => {
					switch (action_flag) {
						case FileMonitorEvent.CREATED:
							dev_files.add (src);
							break;
						case FileMonitorEvent.DELETED:
							dev_files.remove (src);
							break;
						case FileMonitorEvent.MOVED_IN:
							dev_files.add (dst);
							break;
						case FileMonitorEvent.MOVED_OUT:
							dev_files.remove (dst);
							break;
						case FileMonitorEvent.RENAMED:
							dev_files.remove (src);
							dev_files.add (dst);
						default:
							break;
					}
					dev_files.sort (path_sort_function);
				});
			} catch (Error e) {
				warning (@"Filed to search for devices: $(e.message)");
			}
		}
		
		static int path_sort_function (File file1, File file2) {
			return strcmp (file1.get_path (), file2.get_path ());
		}
	}
}
