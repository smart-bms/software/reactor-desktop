namespace ReactorDesktop.View.Widgets {

	[GtkTemplate (ui = "/com/reactor/Desktop/view/widgets/cell_dashboard.ui")]
	public class CellDashboard : Gtk.TreeView {

		int rows = 0;

		Gtk.TreeIter[] iterators;
		RowData[] data;

		public Reactor.Com? _communications;
		public Reactor.Com? communications {
			get {
				return _communications;
			}
			set {
				value.receive_voltage.connect (construct_table);
				value.receive_voltage.connect ((voltage) => {
					for (var i = 0; i < data.length; i++) {
						data[i].voltage = (uint) voltage[i];
					}
					update ();
				});
				value.receive_balancing.connect ((balancing) => {
					for (var i = 0; i < data.length; i++) {
						data[i].balancing = balancing % 2 == 1;
						balancing /= 2;
					}
					update ();
				});
				// TODO connect the rest
			}
		}

		public void update () {
			var model = this.model as Gtk.ListStore;
			assert (model != null);
			for (var i = 0; i < data.length; i++) {
				data[i].update_iter (model, this.iterators[i], /* min voltage */ 3200, /* max voltage */ 4200);
			}
		}

		void construct_table (int16[] voltages) {
			if (rows == voltages.length)
				return;
			rows = voltages.length;
		
			var model = this.model as Gtk.ListStore;
			assert (model != null);
			model.clear ();
			iterators = {};
			data = {};
			uint index = 0;
			foreach (var v in voltages) {
				Gtk.TreeIter iter;
				model.append (out iter);
				iterators += iter;
				set_blank (iter);
				data += RowData (index);
				data[index++].voltage = v;
			}
		}

		void set_blank (Gtk.TreeIter iter) {
			var model = this.model as Gtk.ListStore;
			assert (model != null);
			model.set (iter, 0, 0, 0, false, false, false, 0, -1);
		}

		public void reset () {
			if (communications == null) {
				return;
			}
			
			this.communications = communications;
		}

		struct RowData {
			
			uint index;
			uint voltage;
			bool balancing;
			bool overvoltage;
			bool undervoltage;
			uint intern_res;
			
			public RowData (uint index) {
				this.index = index;
				voltage = 0;
				balancing = false;
				overvoltage = false;
				undervoltage = false;
				intern_res = 0;
			}
			
			public double voltage_fraction (uint min_voltage, uint max_voltage) {
				return ((voltage - min_voltage) * 100) / ((double) max_voltage - min_voltage);
			}
			
			// public string to_string () {
			// 	return @"{'index': $index, 'voltage': $voltage, 'balancing': $balancing, 'overvoltage': $overvoltage, 'undervoltage': $undervoltage, 'intern_res': $intern_res}, voltage_fraction: $(voltage_fraction (3200, 4200))";
			// }
			
			public void update_iter (Gtk.ListStore store, Gtk.TreeIter iter, uint min_voltage, uint max_voltage) {
				var index_value = Value (typeof(uint));
				index_value.set_uint (index);
				
				var voltage_value = Value (typeof(uint));
				voltage_value.set_uint (voltage);
				
				var v_frac_value = Value (typeof(double));
				v_frac_value.set_double (voltage_fraction (min_voltage, max_voltage));
				
				var balancing_value = Value (typeof(bool));
				balancing_value.set_boolean (balancing);

				var overvoltage_value = Value (typeof(bool));
				overvoltage_value.set_boolean (balancing);

				var undervoltage_value = Value (typeof(bool));
				undervoltage_value.set_boolean (balancing);
				
				var intern_res_value = Value (typeof(uint));
				intern_res_value.set_uint (intern_res);
				
				store.set_value (iter, 0, index_value);
				store.set_value (iter, 1, voltage_value);
				store.set_value (iter, 2, v_frac_value);
				store.set_value (iter, 3, balancing_value);
				store.set_value (iter, 4, overvoltage_value);
				store.set_value (iter, 5, undervoltage_value);
				store.set_value (iter, 6, intern_res_value);
			}
		}

        public enum Column {
            ID, VOLTAGE, BALANCING;

            public string to_string () {
                switch (this) {
                    case ID: return "Cell";
                    case VOLTAGE: return "Voltage";
                    case BALANCING: return "Balancing";
                    default: return "";
                }
            }
        }

	    [GtkTemplate (ui = "/com/reactor/Desktop/view/widgets/value_bar.ui")]
	    public class ValueBar : Gtk.Bin {

            public string suffix = "";

            double _value;
            public double @value {
                get {
                    return _value;
                }
                set {
                    _value = value;
                    bar.fraction = (value - 3200)/(4200 - 3200);
                    label.label = @"$value $suffix";
                }
            }

	        [GtkChild]
	        Gtk.Label label;

	        [GtkChild]
	        Gtk.ProgressBar bar;

            public ValueBar (double @value = 0) {
                this.value = value;
            }

	    }

    }

}
