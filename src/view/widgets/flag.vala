/* flag.vala
 *
 * Copyright 2020 Alex Angelou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ReactorDesktop.View.Widgets {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/widgets/flag.ui")]
	public class Flag : Gtk.Bin {

	    [GtkChild]
        Gtk.Image image;

        [GtkChild]
        Gtk.Label label;

        public ThemedIcon icon {
            owned get {
                return image.gicon as ThemedIcon;
            }
            set {
                image.gicon = value;
            }
        }

        public string text {
            get {
                return label.label;
            }
            set {
                label.label = value;
            }
        }

        bool _is_important;
        public virtual bool is_important {
            get {
                return _is_important;
            }
            set {
                if (_is_important == value)
                    return;

                _is_important = value;

                if (value) {
                    get_style_context ().add_class ("dangerous");
                } else {
                    get_style_context ().remove_class ("dangerous");
                }
            }
        }

        protected bool has_icon {
            get {
                return image.visible;
            }
            set {
                image.visible = value;
            }
        }

	}

}

