/* polynomial.vala
 *
 * Copyright 2020 Alex Angelou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ReactorDesktop.View.Widgets {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/widgets/polynomial.ui")]
	public class Polynomial : Gtk.Bin {

		static Regex number;

		[GtkChild]
		Gtk.Stack stack;
		
		[GtkChild]
		Gtk.Box label_box;
		
		[GtkChild]
		Gtk.Label label;
		
		[GtkChild]
		Gtk.Button edit_button;

		[GtkChild]
		Gtk.Frame frame;

		[GtkChild]
		Gtk.Box contents;

		public Reactor.Polynomial polynomial {
			owned get {
				return to_polynomial ();
			}
			set {
				from_polynomial (value);
			}
		}

		public Polynomial () {

		}

		static construct {
			try {
				number = new Regex ("^[0-9]*.[0-9]*$");
			} catch (RegexError e) {
				assert_not_reached ();
			}
		}

		construct {
			edit_button.clicked.connect (() => stack.visible_child = frame);
		
			foreach (var widget in contents.get_children ()) {
				if (! (widget is Gtk.Entry))
					continue;

				var entry = widget as Gtk.Entry;
				entry.insert_text.connect ((new_text, new_textlength) => {
					if (!number.match (new_text)) {
						Signal.stop_emission ((void*) entry, Signal.lookup ("insert_text", typeof (Gtk.Entry)), 0);
					}
				});
				entry.activate.connect (() => {
					update_label ();
					stack.visible_child = label_box;
				});
			}
		}
		
		public Reactor.Polynomial to_polynomial () {
			var _box_children = contents.get_children ();
			var box_children = new Gee.ArrayList<Gtk.Widget> ();
			foreach (var widget in _box_children) {
				box_children.add (widget);
			}
			float[] coefficients = {};
			box_children
				.map<Gtk.Entry?> ((widget) => widget as Gtk.Entry)
				.filter ((widget) => widget != null)
				.map<float?> ((entry) => float.parse (entry.text == ""? "0": entry.text))
				.foreach ((@value) => {
					coefficients += value;
					return true;
				});
			return new Reactor.Polynomial.from_array (coefficients);
		}
		
		public void from_polynomial (Reactor.Polynomial polynomial) {
			uint i = 0;
			foreach (var widget in contents.get_children ()) {
				if (! (widget is Gtk.Entry))
					continue;

				var entry = widget as Gtk.Entry;
				entry.text = polynomial.coeffs[i].to_string ();
			}
		}

		public void update_label () {
			var polynomial = polynomial;
			var power = 0;
			label.label = "";
			foreach (var coeff in polynomial.coeffs) {
				string x;
				switch (power) {
					case 0:
						x = "";
						break;
					case 1:
						x = "x + ";
						break;
					default:
						x = @"x<sup>$power</sup> + ";
						break;
				}
				label.label = @"$coeff $x $(label.label)";
				power++;
			}
		}

	}

}
