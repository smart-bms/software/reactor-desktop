namespace ReactorDesktop.View.Widgets {

	[GtkTemplate (ui = "/com/reactor/Desktop/view/widgets/contents_row.ui")]
	public class ContentsRow : Gtk.ListBoxRow {

		[GtkChild]
		Gtk.Separator separator;

		[GtkChild]
		Gtk.Label label;

		[GtkChild]
		Gtk.Box contents;

		[GtkChild]
		Gtk.Box box;

		public bool show_separator {
			get {
				return separator.visible;
			}
			set {
				separator.visible = value;
			}
		}

		public string label_text {
			get {
				return label.label;
			}
			set {
				label.label = value;
			}
		}

		bool _expand_child = false;
		public bool expand_child {
			get {
				return _expand_child;
			}
			set {
				_expand_child = value;

				box.set_child_packing (label, !value, false, 0, Gtk.PackType.START);

				if (contents == null)
					return;

				foreach (var widget in contents.get_children ()) {
					contents.set_child_packing (widget, value, value, 0, Gtk.PackType.END);
				}
			}
		}
		
		public Pango.EllipsizeMode ellipsize {
			get {
				return label.ellipsize;
			}
			set {
				label.ellipsize = value;
			}
		}

		public override void add (Gtk.Widget widget) {
			if (contents != null) {
				if (!(widget is Gtk.Label)) {
					contents.margin = 8;
				}
				contents.pack_end(widget, expand_child, expand_child, 0);
				return;
			}

			base.add(widget);
		}

		construct {
			assert(contents != null);
		}

	}
}
