/* flag.vala
 *
 * Copyright 2020 Alex Angelou
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ReactorDesktop.View.Widgets {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/widgets/mode_settings.ui")]
    public class ModeSettings : Gtk.Frame {

        [GtkChild]
        Gtk.ListBox list_box;

        public string section_name {get; construct;}

        construct {
            foreach (var widget in list_box.get_children ()) {
                var row = widget as Widgets.ContentsRow;
                if (row == null)
                    continue;
                // row.label_text = row.label_text.replace ("%s", section_name == null? "": section_name);
            }
        }

    }
}

