
namespace ReactorDesktop.View {
	[GtkTemplate (ui = "/com/reactor/Desktop/view/settings_view.ui")]
	public class SettingsView : ContentsView {

		[GtkChild]
		Gtk.Scale capacity_scale;
		
		[GtkChild]
		Gtk.ComboBoxText balancing_selection;
		
		[GtkChild]
		Widgets.Polynomial vitality_polynomial;
		
		[GtkChild]
		Widgets.Polynomial capacity_polynomial;
		
		[GtkChild]
		Gtk.SpinButton ov_spinbutton;

		[GtkChild]
		Gtk.SpinButton uv_spinbutton;

		[GtkChild]
		Gtk.SpinButton ov_rel_spinbutton;

		[GtkChild]
		Gtk.SpinButton uv_rel_spinbutton;

		// [GtkChild]
		// Widgets.ModeSettings charging_panel;

		// [GtkChild]
		// Widgets.ModeSettings discharging_panel;

		[GtkChild]
		Gtk.SpinButton nominal_cap_spinbutton;
		
		[GtkChild]
		Gtk.SpinButton nominal_vol_spinbutton;

		Reactor.Com? _communications = null;
		public override Reactor.Com? communications {
			get {
				return _communications;
			}
			set {
				_communications = value;
			}
		}

		public uint cap_limit {
			get {
				return (int) capacity_scale.adjustment.value;
			}
			set {
				capacity_scale.adjustment.value = (int) value;
			}
		}
		
		public Model.BalancingOptions balancing_mode {
			get {
				return Model.BalancingOptions.from_string (balancing_selection.get_active_text ());
			}
			set {
				balancing_selection.active = (int) value;
			}
		}

		public Reactor.Polynomial vitality_gain {
			owned get {
				return vitality_polynomial.polynomial;
			}
			set {
				vitality_polynomial.polynomial = value;
			}
		}

		public Reactor.Polynomial vol_to_cap {
			owned get {
				return capacity_polynomial.polynomial;
			}
			set {
				capacity_polynomial.polynomial = value;
			}
		}

		public uint ov_limit {
			get {
				return (uint) ov_spinbutton.value;
			}
			set {
				ov_spinbutton.value = (double) value;
			}
		}

		public uint uv_limit {
			get {
				return (uint) uv_spinbutton.value;
			}
			set {
				uv_spinbutton.value = (double) value;
			}
		}

		public uint ov_release {
			get {
				return (uint) ov_rel_spinbutton.value;
			}
			set {
				ov_rel_spinbutton.value = (double) value;
			}
		}

		public uint uv_release {
			get {
				return (uint) uv_rel_spinbutton.value;
			}
			set {
				uv_rel_spinbutton.value = (double) value;
			}
		}

		public uint nominal_capacity {
			get {
				return (uint) nominal_cap_spinbutton.value;
			}
			set {
				nominal_cap_spinbutton.value = (double) value;
			}
		}

		public uint nominal_voltage {
			get {
				return (uint) nominal_vol_spinbutton.value;
			}
			set {
				nominal_vol_spinbutton.value = (double) value;
			}
		}

		Gtk.Button apply_button = new Gtk.Button.with_label ("Apply");

		construct {
			apply_button.get_style_context ().add_class (Gtk.STYLE_CLASS_SUGGESTED_ACTION);
			apply_button.visible = true;
            header_widgets.pack_end (apply_button, false, false, 0);
		}

	}
}
